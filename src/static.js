const fs = require("fs");
const products = require("../data/products.json");
const renderList = require("./renderList.js");

// Generate markup from data

const list = renderList(products);

let content = `
  <!doctype html>
  <head>
  <title>Vegansk Liste</title>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <style>
    body {
      font-family: -apple-system, sans-serif;
    }

    p span {
      font-weight: bold;
    }

    em {
      color: gray;
    }

    input {
      font-size: 1rem;
      width: calc(100% - 8px);
    }
  </style>
  </head>
  <body>
  ${list}
  <script src="bundle.js"></script>
  </body>
`;

// Write content to file

const dir = "www";

if (!fs.existsSync(dir)) {
  fs.mkdirSync(dir);
}

fs.writeFileSync(`${dir}/index.html`, content, "utf8", function(err) {
  if (err) {
    console.error(err);
  }
});
