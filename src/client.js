var yo = require("yo-yo");
var products = require("../data/products.json");
var renderList = require("./renderList.js");
var filter = require("./filter.js");

var body = document.body;
body.insertBefore(document.createElement("header"), body.firstChild);

var header = document.querySelector("header");
header.innerHTML = '<input type="text"  name="filter" placeholder="Søg…" />';

var list = renderList(products);
body.replaceChild(list, document.querySelector("section"));

function updateList(value) {
  yo.update(list, renderList(filter(products, value)));
}

var input = document.querySelector("input");
input.addEventListener(
  "input",
  function(event) {
    updateList(input.value);
  },
  false
);
