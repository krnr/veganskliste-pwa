var yo = require("yo-yo");

function renderList(items) {
  return yo`<section>
    <div>
      ${items.map(function(item) {
        let brand = item.brand ? yo`<span>${item.brand}</span>` : ``;
        let brandDivider = item.brand ? `, ` : ``;
        let dealers = item.dealers
          ? yo`<em> (forhandles i ${item.dealers})</em>`
          : ``;
        return yo`<p>${brand}${brandDivider}${item.name}${dealers}</p>`;
      })}
    </div>
  </section>`;
}

module.exports = renderList;
