let flatten = require("lodash.flatten");

function filter(data, value) {
  let normValue = normalizeString(value);

  if (normValue === "") {
    return data;
  } else {
    return data.filter(function(item) {
      // Create flat array of all item strings
      return (
        flatten(Object.values(item))
          // Normalize all array items
          .map(function(x) {
            return normalizeString(x);
          })
          // Create array of booleans
          .map(function(x) {
            return x.includes(normValue);
          })
          // Return true if array includes true
          .includes(true)
      );
    });
  }
}

function normalizeString(string) {
  return string
    .toLowerCase()
    .replace(/['’]/g, "")
    .replace(/[åäæ]/g, "a")
    .replace(/[øö]/g, "o")
    .replace(/[é]/g, "e")
    .replace(/[ú]/g, "u")
    .trim();
}

module.exports = filter;
